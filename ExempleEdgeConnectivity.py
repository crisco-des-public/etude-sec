# Programme ExempleEdgeConnectivity.py
# Date création : 23 octobre 2020
# Objet : ce programme est un exemple pour illuster la fonction edge_connectivity de la librairie igraph avec le graphe donné en exemple dans la fig1b

import numpy as np
import igraph

# Création de la matrice 10 x 10 avec en intitulés de lignes et colonnes les sommets Ux avec x allant de 1 à 10
M=np.array([
	[0,1,1,1,0,1,0,0,0,0],
	[0,0,0,1,1,0,1,0,0,0],
	[0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,1,1,1,0,0,0],
	[0,0,0,0,0,0,1,0,0,0],
	[0,0,0,0,0,0,0,0,1,1],
	[0,0,0,0,0,0,0,1,1,1],
	[0,0,0,0,0,0,0,0,0,1],
	[0,0,0,0,0,0,0,0,0,1],
	[0,0,0,0,0,0,0,0,0,0]
])

# création du graphe
G=igraph.Graph.Adjacency(M.tolist(),"MAX")

# G.get_edgelist()[6] correspond à l'arête U2-U7
# G.get_edgelist()[13] correspond à l'arête U7-U8

print("edge_connectivity entre U2 et U7 :",G.edge_connectivity(G.get_edgelist()[6][0],G.get_edgelist()[6][1]))

print("edge_connectivity entre U7 et U8 :",G.edge_connectivity(G.get_edgelist()[13][0],G.get_edgelist()[13][1]))